package project.etaskify.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import project.etaskify.controller.NotificationController;
import project.etaskify.dto.TaskDto;
import project.etaskify.enums.TaskStatus;
import project.etaskify.model.AppUser;
import project.etaskify.model.Task;
import project.etaskify.repository.AppUserRepository;
import project.etaskify.repository.TaskRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class TaskService {
private final AppUserRepository appUserRepository;
private final TaskRepository taskRepository;
private final NotificationController notificationController;
private final ModelMapper modelMapper;
@Transactional
    public ResponseEntity<String> createTask(TaskDto dto, List<Long> userIds) {

        for (Long userId : userIds) {
            Optional<AppUser> optionalAppUser = appUserRepository.findById(userId);
            if (optionalAppUser.isPresent()) {
                AppUser appUser = optionalAppUser.get();

                Task task = modelMapper.map(dto, Task.class);
                task.setAppuser(appUser);
                task.setOrganization(appUser.getOrganization());


                appUser.getTasks().add(task);
                taskRepository.save(task);
                notificationController.publish(appUser);

            }

        }





        return ResponseEntity.ok().build();
    }
}
