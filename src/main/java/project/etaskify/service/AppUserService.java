package project.etaskify.service;

import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.websocket.Session;
import lombok.RequiredArgsConstructor;
import org.aspectj.weaver.ast.Or;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import project.etaskify.config.security.BaseJwtService;
import project.etaskify.dto.LoginDto;
import project.etaskify.dto.SignUpDto;
import project.etaskify.dto.SignUpResponseDto;
import project.etaskify.dto.UserCreateDto;
import project.etaskify.enums.ErrorCodes;
import project.etaskify.enums.UserAuthority;
import project.etaskify.exception.BadRequestException;
import project.etaskify.model.AppUser;
import project.etaskify.model.Authority;
import project.etaskify.model.Organization;
import project.etaskify.repository.AppUserRepository;
import project.etaskify.repository.AuthorityRepository;
import project.etaskify.repository.OrganizationRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AppUserService {
    private final AppUserRepository appUserRepository;
    private final AuthorityRepository authorityRepository;
    private final OrganizationRepository organizationRepository;
    private final BaseJwtService baseJwtService;
    private final BCryptPasswordEncoder passwordEncoder;




    public SignUpResponseDto signUp( SignUpDto dto) {

        Optional<AppUser> appUser=appUserRepository.findByUsername(dto.getUsername());

        if (appUser.isPresent()){
            throw new BadRequestException(ErrorCodes.USER_ALREADY_PRESENT);
        }
        if (dto.getOrganizationName() == null || dto.getUsername() == null ||
                dto.getEmail() == null || dto.getPassword() == null) {
          throw   new BadRequestException(ErrorCodes.MISSING_FIELDS);
        }
        if (!dto.getPassword().equals(dto.getRepeatPassword())){
            throw new BadRequestException(ErrorCodes.PASSWORD_DIDNT_MATCH);
        }

        if (organizationRepository.findByName(dto.getOrganizationName()).isPresent()) {
         throw    new BadRequestException(ErrorCodes.ORGANIZATION_NOT_FOUND);
        }

        Organization organization=Organization.builder().name(dto.getOrganizationName()).address(dto.getAddress())
                .phone(dto.getPhoneNumber()).build();
        createOrganization(organization);
        Authority userAuthority=authorityRepository.findByAuthority(UserAuthority.ADMIN).orElseThrow(()->
                new BadRequestException(ErrorCodes.AUTHORITY_NOT_FOUND));



        AppUser userForSave=AppUser.builder().authorities(List.of(userAuthority))
                .password(passwordEncoder.encode(dto.getPassword()))
                .organization(organization)
                .username(dto.getUsername()).name(dto.getName()).email(dto.getEmail())
                .repeatPassword(passwordEncoder.encode(dto.getRepeatPassword()))
                .surname(dto.getSurname()).build();

        appUserRepository.save(userForSave);

        return  SignUpResponseDto.builder().jwt(baseJwtService.issueToken(userForSave)).build();
    }

    public void createOrganization(Organization organization) {
        Organization existingOrganization = organizationRepository.findByName(organization.getName()).orElse(null);

        if (existingOrganization != null) {
            existingOrganization.setName(organization.getName());
            existingOrganization.setPhone(organization.getPhone());
            existingOrganization.setAddress(organization.getAddress());

            organizationRepository.save(existingOrganization);
        }
    }








@Transactional
    public SignUpResponseDto registerUser(UserCreateDto dto) {

        Optional<AppUser> appUser=appUserRepository.findByUsername(dto.getUsername());

        if (appUser.isPresent()){
            throw new BadRequestException(ErrorCodes.USER_ALREADY_PRESENT);
        }
        if (dto.getOrganizationName() == null || dto.getUsername() == null ||
                dto.getEmail() == null || dto.getPassword() == null) {
            throw   new BadRequestException(ErrorCodes.MISSING_FIELDS);
        }
        if (!dto.getPassword().equals(dto.getRepeatPassword())){
            throw new BadRequestException(ErrorCodes.PASSWORD_DIDNT_MATCH);
        }

      Optional<Organization> organization=organizationRepository.findByName(dto.getOrganizationName());
        if(!organization.isPresent()){
            throw new BadRequestException(ErrorCodes.ORGANIZATION_NOT_FOUND);
        }

    Authority authority;
    Optional<Authority> userAuthority = authorityRepository.findByAuthority(UserAuthority.USER);
    if (userAuthority.isPresent()) {
        authority = userAuthority.get();
    } else {
        authority = new Authority();
        authority.setAuthority(UserAuthority.USER);
        authorityRepository.save(authority);
    }

        organizationRepository.saveAndFlush(organization.get());

        AppUser userForSave=AppUser.builder().authorities(List.of(authority))
                .password(passwordEncoder.encode(dto.getPassword()))
                .organization(organization.get())
                .username(dto.getUsername()).name(dto.getName()).email(dto.getEmail())
                .repeatPassword(passwordEncoder.encode(dto.getRepeatPassword()))
                .surname(dto.getSurname()).build();





        appUserRepository.save(userForSave);

        return  SignUpResponseDto.builder().jwt(baseJwtService.issueToken(userForSave)).build();


    }


    public SignUpResponseDto login(LoginDto dto) {
        Optional<AppUser> optionalUser = appUserRepository.findByUsername(dto.getUsername());

        if (optionalUser.isPresent()) {
            AppUser user = optionalUser.get();
            if (passwordEncoder.matches(dto.getPassword(), user.getPassword())) {
                String jwtToken = baseJwtService.issueToken(user);
                return SignUpResponseDto.builder().jwt(jwtToken).build();
            }
        }

        throw new BadRequestException(ErrorCodes.USERNAME_OR_PASS_INCORRECT);
    }
}

