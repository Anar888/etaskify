package project.etaskify.exception;

import lombok.Getter;
import project.etaskify.enums.ErrorCodes;

@Getter
public class BadRequestException extends RuntimeException{
    private final ErrorCodes errorCodes;
    private   final transient Object[] arguments;

    public BadRequestException(ErrorCodes errorCodes, Object ... args) {
        this.errorCodes = errorCodes;
        this.arguments=args==null ?new Object[0]:args;
    }
}
