package project.etaskify.exception;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import project.etaskify.dto.ErrorResponseDTO;

import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;

@ControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    private  final TranslationServiceImpl translationService;
    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorResponseDTO> handleBadRequestException(BadRequestException ex, WebRequest req){
        ex.printStackTrace();
        var lang =req.getHeader(ACCEPT_LANGUAGE)== null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
        return ResponseEntity.status(400).body(ErrorResponseDTO.builder()
                .status(400).title("Exception")
                .details(translationService.findByKey(ex.getErrorCodes().name(),lang,ex.getArguments())).build());

    }


}
