package project.etaskify.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserCreateDto {
    private String organizationName;
    private String username;
    private String name;
    private String surname;
    private String email;
    @Size(min = 6, message = "Şifre en az 6 simvol olmalıdır.")
    private String password;
    @Size(min = 6, message = "Şifre en az 6 simvol olmalıdır.")
    private String repeatPassword;

}
