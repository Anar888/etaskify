package project.etaskify.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import project.etaskify.model.Authority;
import project.etaskify.model.Task;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignUpDto {
        private String organizationName;
        private String phoneNumber;
        private String address;
        private String username;
        private String name;
        private String surname;
        private String email;
        @Size(min = 6, message = "Şifre en az 6 simvol olmalıdır.")
        private String password;
        @Size(min = 6, message = "Şifre en az 6 simvol olmalıdır.")
        private String repeatPassword;
}
