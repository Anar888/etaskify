package project.etaskify;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import project.etaskify.enums.UserAuthority;
import project.etaskify.model.AppUser;
import project.etaskify.model.Authority;
import project.etaskify.model.Organization;
import project.etaskify.repository.AppUserRepository;
import project.etaskify.repository.AuthorityRepository;
import project.etaskify.repository.OrganizationRepository;

import java.util.Optional;

@SpringBootApplication
@EnableFeignClients
@RequiredArgsConstructor
public class EtaskifyApplication implements CommandLineRunner {
	private final AuthorityRepository authorityRepository;
	private final OrganizationRepository organizationRepository;
	private final AppUserRepository appUserRepository;
	public static void main(String[] args) {
		SpringApplication.run(EtaskifyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		Authority authority=new Authority();
//		authority.setAuthority(UserAuthority.USER);
//		authorityRepository.save(authority);

//		Organization organization=new Organization();
//
//		organization.setName("IngressAcademy");
//		organization.setAddress("Bulbul pr");
//		organization.setPhone("+99450352367");
//organizationRepository.save(organization);

//		System.out.println(appUserRepository.findById(3l));


	}
}
