package project.etaskify.config.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@EnableWebSecurity
@Configuration
@RequiredArgsConstructor
public class SecurityConfig {
    private final TokenAuthService tokenAuthService;


    @Bean
    public SecurityFilterChain config(HttpSecurity http) throws Exception{

        http.csrf(csrf -> csrf.disable());


        http.authorizeHttpRequests(auth-> auth.requestMatchers("/user/*").permitAll());
        http.authorizeHttpRequests(auth-> auth.requestMatchers("/publish/create").permitAll());
        http.authorizeHttpRequests(auth-> auth.requestMatchers("/task/*").permitAll());
        http.authorizeHttpRequests(auth-> auth.requestMatchers("/user/userregister").hasAuthority("ADMIN"));

        http.apply(new AuthFilterConfigurerAdapter(tokenAuthService));
        return http.build();
    }
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return  new BCryptPasswordEncoder();
    }
}
