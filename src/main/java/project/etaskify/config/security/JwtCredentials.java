package project.etaskify.config.security;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@FieldNameConstants
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Configuration
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JwtCredentials {

    String sub;
    Long iat;
    Long exp;
    List<String> authority;

}
