package project.etaskify.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.etaskify.dto.LoginDto;
import project.etaskify.dto.SignUpDto;
import project.etaskify.dto.SignUpResponseDto;
import project.etaskify.dto.UserCreateDto;
import project.etaskify.model.AppUser;
import project.etaskify.repository.AppUserRepository;
import project.etaskify.service.AppUserService;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class AppUserController {
private final AppUserService appUserService;

@PostMapping("/signup")
public ResponseEntity<SignUpResponseDto> signUp(@Valid @RequestBody SignUpDto dto) {



    return ResponseEntity.ok(appUserService.signUp(dto));
}

@PostMapping("/userregister")
    public ResponseEntity<SignUpResponseDto> registerUser(@Valid @RequestBody UserCreateDto dto){

    return ResponseEntity.ok(appUserService.registerUser(dto));
}
@GetMapping("/login")
    public ResponseEntity<SignUpResponseDto> login(@RequestBody LoginDto dto){

    return ResponseEntity.ok(appUserService.login(dto));

}


}
