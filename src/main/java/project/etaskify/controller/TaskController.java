package project.etaskify.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.etaskify.dto.TaskDto;
import project.etaskify.repository.TaskRepository;
import project.etaskify.service.TaskService;

import java.util.List;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @PostMapping("/create")
    public ResponseEntity<String> createTask(@RequestBody TaskDto dto, @RequestParam List<Long> userIds){

     return  taskService.createTask(dto,userIds);

    }
}
