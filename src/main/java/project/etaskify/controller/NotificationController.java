package project.etaskify.controller;

import lombok.RequiredArgsConstructor;

import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.etaskify.model.AppUser;


@RestController
@RequestMapping("/publish")
@RequiredArgsConstructor
public class NotificationController {

    private final KafkaTemplate<String,Object> kafkaTemplate;


    @PostMapping("/create")
    public ResponseEntity<Void> publish(@RequestBody AppUser user){


        kafkaTemplate.send("Email-topic",null,user);
        return ResponseEntity.ok().build();
    }
}
