package project.etaskify.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@FieldNameConstants
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "organization")
public class Organization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String phone;
    private String address;
    @OneToMany(
            mappedBy = "organization",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL

    )
    private List<AppUser> appusers = new ArrayList<>();
    @OneToMany(
            mappedBy = "organization",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL
    )
    private List<Task> tasks = new ArrayList<>();

    @Override
    public String toString() {
        return "Organization{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
