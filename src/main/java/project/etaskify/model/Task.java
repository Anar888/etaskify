package project.etaskify.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import project.etaskify.enums.TaskStatus;

import java.time.LocalDateTime;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "task")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String description;
    private LocalDateTime deadline;
    private TaskStatus status;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private AppUser appuser;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private Organization organization;

}
