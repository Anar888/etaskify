package project.etaskify.enums;

public enum TaskStatus {
    TODO,
    IN_PROGRESS,
    COMPLETED
}
