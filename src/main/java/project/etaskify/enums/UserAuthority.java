package project.etaskify.enums;

public enum UserAuthority {
    ADMIN,
    USER
}
