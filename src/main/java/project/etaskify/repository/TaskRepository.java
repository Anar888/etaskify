package project.etaskify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.etaskify.model.AppUser;
import project.etaskify.model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task,Long> {




}
