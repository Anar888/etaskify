package project.etaskify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.etaskify.enums.UserAuthority;
import project.etaskify.model.Authority;

import java.util.Optional;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority,Long> {
    Optional<Authority> findByAuthority(UserAuthority userAuthority);
}
